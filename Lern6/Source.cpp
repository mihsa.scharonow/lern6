#include<iostream>
#include <string>

class Player
{
private:
	int point;
	std::string Name;
public:


	Player():point(0), Name("Void")
	{}

	Player(int k, std::string NameP = "Void") : point(k), Name(NameP)
	{}

	//operator priswaeniya
	Player& operator=(Player& k)
	{
		point = k.point;
		Name = k.Name;
		return *this;
	}

	// operator swapa
	Player& operator<<(Player& k)
	{
		Player Temp(point, Name);
		point = k.point;
		Name = k.Name;
		k.Name = Temp.Name;
		k.point = Temp.point;
		return *this;
	}

	Player EditData(int p, std::string N)
	{
		point = p;
		Name = N;
		return *this;
	}


	void print()
	{
		std::cout << "Point: " << point << "  Name: " << Name << '\n';
	}

	void printList(Player* List, int CostPlayer)
	{
		for (int i = 0; i < CostPlayer; i++)
		{
			List[i].print();
		}
	}

	void sortPoint(Player* List, int CostPlayer)
	{
		
		if (CostPlayer <= 1) return;
		bool b = true;
		while (b)
		{
			b = false;
			for (int i = 0; i < CostPlayer - 1; i++)
			{
				if (List[i].point > List[i + 1].point)
				{
					List[i] << List[i + 1];
					b = true;
				}
			}
		}
	}

};

int main()
{
	int CostPlayer;
	int k;
	std::string Name;
	Player Temp(8, "Karina");
	std::cout << "Cost Player: ";
	std::cin >> CostPlayer;
	Player* ListPlayer = new Player [CostPlayer];

	for (int i = 0; i < CostPlayer; i++)
	{
		std::cout << "Player point " << i << " :";
		std::cin >> k;
		std::cout << "Name point " << i << " :";
		std::cin >> Name;
		ListPlayer[i].EditData(k, Name);
	}
	std::cout << "List Player: " << '\n';
	ListPlayer->printList(ListPlayer, CostPlayer);

	std::cout << "Sortirovka: " << '\n';
	ListPlayer->sortPoint(ListPlayer, CostPlayer);
	ListPlayer->printList(ListPlayer, CostPlayer);

	delete [] ListPlayer;

}